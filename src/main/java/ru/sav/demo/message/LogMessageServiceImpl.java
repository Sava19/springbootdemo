package ru.sav.demo.message;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.sav.demo.controllers.Message;

@Service
@Profile("test")
public class LogMessageServiceImpl implements MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogMessageServiceImpl.class);

    @Override
    public String save(Message message) {
        LOGGER.info("message: {}", message);
        return null;
    }

    @Override
    public Message getId(String id) {
        LOGGER.info("get id: {}", id);
        return null;
    }
}
