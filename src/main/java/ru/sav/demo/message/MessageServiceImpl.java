package ru.sav.demo.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.sav.demo.controllers.Message;
import ru.sav.demo.controllers.MyController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Profile("local")
public class MessageServiceImpl implements MessageService {

    private final Map<String, Message> messages = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Override
    public String save(Message message) {

        String id = UUID.randomUUID().toString();
        messages.put(id, message);
        return id;
    }

    @Override
    public Message getId(String id) {
        return messages.get(id);
    }
}
