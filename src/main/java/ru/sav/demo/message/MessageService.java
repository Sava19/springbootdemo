package ru.sav.demo.message;

import ru.sav.demo.controllers.Message;


public interface MessageService {
    String save(Message message);


    Message getId(String id);
}
