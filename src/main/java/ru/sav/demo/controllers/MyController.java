package ru.sav.demo.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.sav.demo.message.MessageService;

import javax.validation.Valid;

@RestController
@RequestMapping("/demo")
public class MyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyController.class);

    private final MessageService service;

    @Value("${myProp}")
    private String myValue;

    public MyController(MessageService service) {
        this.service = service;
    }

    @PostMapping("/save")
    public String saveMessage(@RequestBody @Valid Message message) {
        LOGGER.info("message: {}", message);
        return service.save(message);
    }

    @GetMapping("/getMessage/{id}")
    public Message getMessage(@PathVariable String id) {
        LOGGER.info("id: {}", id);
        return service.getId(id);
    }

    @GetMapping("/getSettings")
    public String getSettings() {
        return myValue;
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void test() {

    }

}
