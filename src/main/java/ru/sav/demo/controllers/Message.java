package ru.sav.demo.controllers;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public class Message {

    public Message() {
    }

    @NotNull
    private String text;

    public String getText() {
        return text;
    }

    public Message(@NotNull String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
