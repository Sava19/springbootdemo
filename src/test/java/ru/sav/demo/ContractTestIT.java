package ru.sav.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.sav.demo.controllers.Message;
import ru.sav.demo.message.MessageService;

import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class ContractTestIT {

    @MockBean
    private MessageService messageService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void saveMessage200() throws Exception {
        String id = UUID.randomUUID().toString();
        when(messageService.save(any())).thenReturn(id);
        mockMvc.perform(post("/demo/save").content("{\"text\":\"Text\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void saveMessage400() throws Exception {
        String id = UUID.randomUUID().toString();
        when(messageService.save(any())).thenReturn(id);
        mockMvc.perform(post("/demo/save").content("{}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void saveMessage500() throws Exception {
        String id = UUID.randomUUID().toString();
        when(messageService.save(any())).thenThrow(new RuntimeException());
        mockMvc.perform(post("/demo/save").content("{\"text\":\"Text\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

}